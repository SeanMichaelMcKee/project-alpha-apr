from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_home(request):
    return redirect("/projects/")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("", redirect_to_home, name="home"),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
]
